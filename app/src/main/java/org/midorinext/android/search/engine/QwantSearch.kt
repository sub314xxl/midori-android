package org.midorinext.android.search.engine

import org.midorinext.android.R

class QwantSearch: BaseSearchEngine(
        iconUrl ="file:///android_asset/baidu.webp",
        queryUrl = "https://www.qwant.com/?q=%s",
        R.string.search_engine_qwant
)