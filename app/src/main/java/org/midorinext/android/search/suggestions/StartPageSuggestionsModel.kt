package org.midorinext.android.search.suggestions

import android.app.Application
import io.reactivex.Single
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import org.midorinext.android.R
import org.midorinext.android.constant.UTF8
import org.midorinext.android.database.SearchSuggestion
import org.midorinext.android.extensions.map
import org.midorinext.android.extensions.preferredLocale
import org.midorinext.android.log.Logger
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory

class StartPageSuggestionsModel(
    okHttpClient: Single<OkHttpClient>,
    requestFactory: RequestFactory,
    application: Application,
    logger: Logger,
): BaseSuggestionsModel(okHttpClient, requestFactory, UTF8, application.preferredLocale, logger) {

    private val searchSubtitle= application.getString(R.string.suggestion)
    //"https://www.startpage.com/sp/search?query=&segment=startpage.midori"
    override fun createQueryUrl(query: String, language: String): HttpUrl = HttpUrl.Builder()
            .scheme("https")
            .host("startpage.com")
            .encodedPath("/sp/search")
            .addEncodedQueryParameter("query", query)
            .addQueryParameter("segment", "startpage.midori")
            .build()

    @Throws(Exception::class)
    override fun parseResults(responseBody: ResponseBody): List<SearchSuggestion> {
        TODO("Not yet implemented")


        }

}