package org.midorinext.android.database

sealed class SettingsKeys(
        open val key: String,
        open val value: String
)