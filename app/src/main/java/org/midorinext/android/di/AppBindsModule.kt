package org.midorinext.android.di

import org.midorinext.android.adblock.allowlist.AllowListModel
import org.midorinext.android.adblock.allowlist.SessionAllowListModel
import org.midorinext.android.adblock.source.AssetsHostsDataSource
import org.midorinext.android.adblock.source.HostsDataSource
import org.midorinext.android.adblock.source.HostsDataSourceProvider
import org.midorinext.android.adblock.source.PreferencesHostsDataSourceProvider
import org.midorinext.android.database.adblock.HostsDatabase
import org.midorinext.android.database.adblock.HostsRepository
import org.midorinext.android.database.allowlist.AdBlockAllowListDatabase
import org.midorinext.android.database.allowlist.AdBlockAllowListRepository
import org.midorinext.android.database.bookmark.BookmarkDatabase
import org.midorinext.android.database.bookmark.BookmarkRepository
import org.midorinext.android.database.downloads.DownloadsDatabase
import org.midorinext.android.database.downloads.DownloadsRepository
import org.midorinext.android.database.history.HistoryDatabase
import org.midorinext.android.database.history.HistoryRepository
import org.midorinext.android.ssl.SessionSslWarningPreferences
import org.midorinext.android.database.javascript.JavaScriptDatabase
import org.midorinext.android.database.javascript.JavaScriptRepository
import org.midorinext.android.ssl.SslWarningPreferences
import dagger.Binds
import dagger.Module

/**
 * Dependency injection module used to bind implementations to interfaces.
 */
@Module
abstract class AppBindsModule {

    @Binds
    abstract fun provideBookmarkModel(bookmarkDatabase: BookmarkDatabase): BookmarkRepository

    @Binds
    abstract fun provideDownloadsModel(downloadsDatabase: DownloadsDatabase): DownloadsRepository

    @Binds
    abstract fun providesHistoryModel(historyDatabase: HistoryDatabase): HistoryRepository

    @Binds
    abstract fun providesJavaScriptModel(javaScriptDatabase: JavaScriptDatabase): JavaScriptRepository

    @Binds
    abstract fun providesAdBlockAllowListModel(adBlockAllowListDatabase: AdBlockAllowListDatabase): AdBlockAllowListRepository

    @Binds
    abstract fun providesAllowListModel(sessionAllowListModel: SessionAllowListModel): AllowListModel

    @Binds
    abstract fun providesSslWarningPreferences(sessionSslWarningPreferences: SessionSslWarningPreferences): SslWarningPreferences

    @Binds
    abstract fun providesHostsDataSource(assetsHostsDataSource: AssetsHostsDataSource): HostsDataSource

    @Binds
    abstract fun providesHostsRepository(hostsDatabase: HostsDatabase): HostsRepository

    @Binds
    abstract fun providesHostsDataSourceProvider(preferencesHostsDataSourceProvider: PreferencesHostsDataSourceProvider): HostsDataSourceProvider
}
