package org.midorinext.android.intro

import android.Manifest
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.github.appintro.SlidePolicy
import org.midorinext.android.AppTheme
import org.midorinext.android.R
import org.midorinext.android.di.injector
import org.midorinext.android.preference.UserPreferences
import javax.inject.Inject


class PermsFragment : Fragment() {

    @Inject
    lateinit var userPreferences: UserPreferences

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.perms, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var col: Int? = null
        when (userPreferences.useTheme) {
            AppTheme.LIGHT -> col = Color.WHITE
            AppTheme.DARK -> col = Color.BLACK
            AppTheme.BLACK -> col = Color.BLACK
        }
        var textCol: Int? = null
        when (userPreferences.useTheme) {
            AppTheme.LIGHT -> textCol = Color.BLACK
            AppTheme.DARK -> textCol = Color.WHITE
            AppTheme.BLACK -> textCol = Color.WHITE
        }

        if (col != null) {
            requireView().setBackgroundColor(col)
        }
        if (textCol != null) {
            requireView().findViewById<TextView>(R.id.permissionsTitle).setTextColor(textCol)
            requireView().findViewById<TextView>(R.id.permissionsDescription).setTextColor(textCol)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injector.inject(this)
    }

    companion object {
        fun newInstance() : PermsFragment {
            return PermsFragment()
        }
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }
}